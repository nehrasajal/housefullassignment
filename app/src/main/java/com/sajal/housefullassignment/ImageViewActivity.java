package com.sajal.housefullassignment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ImageViewActivity extends AppCompatActivity {
    @InjectView(R.id.image_view_view_pager)
    ViewPager viewPager;
    private int currentIndex;
    private ArrayList<String> imagePaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent() != null) {
            currentIndex = getIntent().getIntExtra("current_index", 0);
            imagePaths = getIntent().getStringArrayListExtra("imagepaths");
        }
        viewPager.setAdapter(new ImageViewPagerAdapter(this, imagePaths));
        viewPager.setCurrentItem(currentIndex);
    }
}