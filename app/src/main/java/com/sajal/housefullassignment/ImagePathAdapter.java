package com.sajal.housefullassignment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by SajalHCS on 12/24/15.
 */
public class ImagePathAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final int resource;
    private final List<String> imagePaths;

    public ImagePathAdapter(Context context, int resource, List<String> imagePaths) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        this.imagePaths = imagePaths;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ItemViewHolder itemViewHolder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
            convertView = layoutInflater.inflate(resource, parent, false);
            itemViewHolder = new ItemViewHolder(convertView);
            convertView.setTag(itemViewHolder);
        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        itemViewHolder.imagePath.setText(imagePaths.get(position));
        return convertView;
    }

    @Override
    public int getCount() {
        return imagePaths.size();
    }

    static class ItemViewHolder {
        @InjectView(R.id.list_item_text_view)
        TextView imagePath;

        public ItemViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
